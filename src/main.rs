extern crate chrono;
extern crate clap;
extern crate colored;

use std::{fmt, fs};
use std::fmt::{Error, Formatter};
use std::io::{self, BufWriter, Write};
use std::path::Path;
use std::process::{Command, Stdio};
use std::time::SystemTime;

use chrono::DateTime;
use chrono::offset::Local;
use clap::{App, Arg, SubCommand};
use colored::*;

fn main() -> Result<(), std::io::Error> {
    let matches = App::new("cmdline-utils")
        .version("0.1.0")
        .about("Assorted utility functions")
        .author("Colin Webb")
        .subcommand(
            SubCommand::with_name("llg")
                .about("Like `ls -l` but checks if directories are git roots")
                .arg(Arg::with_name("path").help("the path to use. defaults to current directory").index(1))
            // todo - sort by name/modified_time/git-root-status
        )
        .get_matches();

    match matches.subcommand_name() {
        Some("llg") => {
            let path = matches.subcommand_matches("llg").unwrap()
                .value_of("path").unwrap_or(".");
            llg(Path::new(path))
        }
        _ => {
            println!("Unrecognised sub-command. Please run again with --help");
            Ok(())
        }
    }
}

#[derive(Debug)]
struct Info {
    path: String,
    is_directory: bool,
    is_git_root: bool,
    modified_time: SystemTime,
}

impl fmt::Display for Info {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        let dir_output = if self.is_directory { "d" } else { "f" };
        let git_root = if self.is_git_root { "git" } else { "   " };
        let datetime: DateTime<Local> = self.modified_time.into();
        write!(f, "{} {} {} {}", dir_output, git_root.green(), datetime.format("%d-%b-%Y %R"), self.path)
    }
}

/// Like `ls -l` but also checks if sub directories are in git-roots. If the `path` is in
/// a git-root, then it short-circuits to mark everything as inside a git root.
fn llg(path: &Path) -> Result<(), std::io::Error> {
    let short_circuit = is_in_git_root(path)?;

    let mut results: Vec<_> = fs::read_dir(path)?
        .map(|entry| { entry.unwrap() })
        .filter(|entry| { entry.path().file_name().is_some() })
        .map(move |entry|{
            let metadata = fs::metadata(entry.path()).unwrap();
            let is_git_root = short_circuit || (metadata.is_dir() && is_in_git_root(&entry.path()).unwrap());

            // this is checked to be safe in the `.filter` above, hopefully
            let path = entry.path().clone().file_name().unwrap().to_str().unwrap().to_string();
            Info {
                path,
                is_directory: metadata.is_dir(),
                is_git_root,
                modified_time: metadata.modified().unwrap(),
            }
        })
        .collect();

    results.sort_by(|x, y| x.path.to_lowercase().cmp(&y.path.to_lowercase()));
    let mut handle = BufWriter::new(io::stdout());
    for res in results {
        writeln!(handle, "{}", res)?;
    }
    return handle.flush()
}

/// Evaluates whether a path belongs inside a git root, by invoking `git rev-parse`
/// `git rev-parse --git-dir` will return a status code of zero if it is inside a git root
fn is_in_git_root(path: &Path) -> Result<bool, std::io::Error> {
    let output = Command::new("git")
        .args(&["rev-parse", "--git-dir"])
        .current_dir(path)
        .stdout(Stdio::null())
        .stderr(Stdio::null())
        .status()
        .expect("Failed to execute process");

    Ok(output.success())
}
