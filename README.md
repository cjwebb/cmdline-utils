# cmdline-utils

Some common functions that I often need, but are too complicated to write in Bash (in my opinion).

### List git roots in path

    cmdline-utils llg <path>

Works a bit like `ls -l <path>` but checks to see if the directory is a git root.

    $ ./cmdline-utils llg .
    d git 22-Apr-2020 11:22 .git
    f git 17-Feb-2020 17:13 .gitignore
    d git 21-Feb-2020 08:00 .idea
    f git 17-Feb-2020 07:47 Cargo.lock
    f git 17-Feb-2020 07:47 Cargo.toml
    f git 21-Feb-2020 07:47 README.md
    d git 21-Feb-2020 07:33 src
    d git 22-Apr-2020 11:21 target

| Column | Description |
|:------:|-------------|
| 1      | Indicates file or directory |
| 2      | Indicates if the item is a git root, or inside a git root. This column will be blank if false |
| 3      | modified date |
| 4      | modified time |
| 5      | File / directory name |

If the `<path>` directory itself is a git root, then every line in the output will have the `git` flag; It short-circuits the check, and just lists the contents of `<path>`.

Notes - I find this useful to clear up any frequently-used workspace, with accumulates cruft from one-off investigations and experiments.

### Todo

Add some cmdline options:
 - to only show file/directory names (suggested argument: q)
 - to only output directories (suggested argument: d)
 - to only show things that are non-git (suggested argument: --non-git)

Ideal usage:

    llg -dq --non-git | xargs rm -r
